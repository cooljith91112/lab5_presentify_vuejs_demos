var Vue = require('vue');
var vue_resource = require('vue-resource');


Vue.use(vue_resource);

var test = new Vue({
	el:'#demo',
	data:{
		books:"Kool",
		searchData:"",
		loading:false
	},
	ready: function() {

     
    },
    methods:{
    	doThat:function(){
    		console.log("Kool");
    		this.loading = true;
    		 // GET request
	      	this.$http.get('http://it-ebooks-api.info/v1/search/'+this.searchData).then(function (response) {

		          // get status
		          response.status;

		          // get all headers
		          response.headers();

		          // get 'expires' header
		          response.headers('expires');

		          // set data on vm
		          this.$set('books',response.data);
		          this.$set('loading',false);
		      }, function (response) {

		          // error callback
	      	});

    	}
    }
});